#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib import pyplot as plt
plt.rcParams['text.usetex'] = True
#plt.rcParams['text.latex.preamble'] = [r'\usepackage{bm}']

import math
import numpy as np
def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(7, 3))
	plt.subplots_adjust(left=0.1, right=0.99, top=0.81, bottom=0.14, wspace=0.27, hspace=0.3)
	
	methods		= set([])
	data_sizes	= set([])
	for method,n in buckets:
		methods.add(method)
		data_sizes.add(n)
	methods		= sorted(list(methods))
	data_sizes	= sorted(list(data_sizes))
	M,N = len(methods),len(data_sizes)

	for i,method in enumerate(methods):
		for j,n in enumerate(data_sizes):
			ax = fig.add_subplot(M, N, i*N+j+1)

			layers	= set([])
			dims	= set([])
			for l,d in buckets[method,n]:
				layers.add(l)
				dims.add(d)
			layers	= sorted(list(layers))
			dims	= sorted(list(dims))

			min_value = +math.inf
			max_value = -math.inf
			for k,d in enumerate(dims):

				y,e = [],[]
				for l in layers:
					if (l,d) not in buckets[method,n]:
						y.append(0)
						e.append(0)
						continue
					
					values = buckets[method,n][l,d]

					y.append(np.mean(values))
					e.append(np.std(values))

				x = [l*(math.pow(1.06, k-1.5)) for l in layers]
				ax.errorbar(x, y, yerr=e, color=cm.brg(k/6),label=f'd={d}',linewidth=0.8, capsize=1.5, capthick=0.5)
				max_value = max(max_value, max(y))
				min_value = min(min_value, min(y))

			ax.set_xlim(layers[0]/1.4, layers[-1]*1.4)
			ax.set_ylim(0,max_value*1.5)
			ax.set_xscale('log')

			if i==0 and j==0:
				ax.legend(loc=(1.0, 1.14),ncol=4,fontsize=8, title=r'Number of Hidden Dimmension')

			if i==1: ax.set_xlabel(r'Number of Layers', fontsize=10, labelpad=0.05)
			if j==0: ax.set_ylabel(r'Elapse Times (sec)', fontsize=10)
			
			ax.spines['right'].set_visible(False)
			ax.spines['top'].set_visible(False)

			ax.yaxis.set_ticks_position('left')
			ax.xaxis.set_ticks_position('bottom')

			for L in layers:
				ax.axvline(x=L,color='black',zorder=0,lw=0.5,linestyle='--')
			ax.set_xticks(layers)
			ax.set_xticklabels(layers)

			ax.minorticks_off()

			ax.text(0.55,0.9,f'{method},n={n}',horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,backgroundcolor='white')

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)


# -------------------------------------------------------------------
import os
import json
from collections import defaultdict
def load_results(archive='ARCHIVEs/'):
	if not os.path.exists(archive): return
	keywords = ['model','num_layer','num_dim','num_data']

	buckets = {}
	for d in os.listdir(archive):
		if '.DS' in d: continue

		with open(f'{archive}/{d}/elapsed-times.json', 'r') as fp:
			elapsed_times = json.load(fp)

		with open(f'{archive}/{d}/config.json','r') as fp:
			config = json.load(fp)
		m,l,d,n = [config[k] for k in keywords]

		if m not in buckets: buckets[m] = {}

		for method in ['naive','proposal']:
			if (method,n) not in buckets[m]: buckets[m][method,n] = defaultdict(list)
			buckets[m][method,n][l,d].append(elapsed_times[method])
	return buckets

# -------------------------------------------------------------------
def main():
	report('start')

	buckets = load_results()
	for model in buckets:
		output_file = f"+VIEW/how-accelerate-calculation-with-{model}.png"
		draw(buckets[model], output_file)

	report('finish')


# -------------------------------------------------------------------
if __name__=='__main__':
	main()


