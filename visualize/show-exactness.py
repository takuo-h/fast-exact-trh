#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib import pyplot as plt
plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = r'\usepackage{bm}'

def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(3.5,3.7))
	plt.subplots_adjust(left=0.17, right=0.99, top=0.92, bottom=0.15)

	ax = fig.add_subplot(1,1,1)
	ax.set_aspect('equal')

	x = buckets['proposal']
	y = buckets['naive']
	ax.scatter(x, y, s=100, color='red', zorder=2, marker='+', lw=0.5)

	max_value = max(max(x),max(y))
	min_value = min(min(x),min(y))

	ax.set_xlim(min_value/1.5,max_value*1.5)
	ax.set_ylim(min_value/1.5,max_value*1.5)

	ax.set_xlabel(r'Proposal Tr$[\bm{H}]$', fontsize=15)
	ax.set_ylabel(r'Naive Tr$[\bm{H}]$', fontsize=15)

	ax.spines['right'].set_visible(False)
	ax.spines['top'].set_visible(False)

	ax.yaxis.set_ticks_position('left')
	ax.xaxis.set_ticks_position('bottom')

	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.minorticks_off()

	for t in [1e-1,1e-0,1e+1,1e+2,1e+3,1e+4,1e+5,1e+6]:
		ax.axhline(y=t,color='black',zorder=0,lw=0.5,linestyle='--')
		ax.axvline(x=t,color='black',zorder=0,lw=0.5,linestyle='--')
	ax.plot([0,10**6],[0,10**6],color='black',zorder=0,lw=1.0)


	ax.set_title(r'Dots Denote Different Trials/Hyper-Params', fontsize=12, x=0.45)
		
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.style.use('default');plt.clf();plt.cla();plt.close()


# -------------------------------------------------------------------
import os
import json
from collections import defaultdict
def load_results(archive='ARCHIVEs/'):
	if not os.path.exists(archive): return

	keywords = ['model','num_layer','num_dim','num_data']

	buckets = defaultdict(list)
	for d in os.listdir(archive):
		if '.DS' in d: continue

		with open(f'{archive}/{d}/config.json','r') as fp:
			config = json.load(fp)
		config = (config[k] for k in keywords)

		with open(f'{archive}/{d}/trH.json', 'r') as fp:
			estimator = json.load(fp)

		for key in estimator:
			buckets[key].append(estimator[key])
	return buckets

# -------------------------------------------------------------------
def main():
	report('start draw')	

	buckets = load_results()
	
	output_file = f"+VIEW/how-exact-proposal.png"
	draw(buckets, output_file)

	report('finish')	
	print('-'*50)


# -------------------------------------------------------------------
if __name__=='__main__':
	main()


