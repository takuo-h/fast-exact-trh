python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python experiment/run.py
python visualize/run.py
deactivate
rm -r .venv