 #!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import itertools
def get_parallel_comb(hypara:dict) ->list:
	configs = []
	keys = list(hypara.keys())
	for values in itertools.product(*[hypara[k] for k in keys]):
		config = {key:value for key,value in zip(keys,values)}
		configs.append(config)
	return configs

def get_config():
	report('get model-config1')
	hypara = {}
	hypara['script']		= ['experiment/main.py'] 

	hypara['num_data']		= [10,100,1000]
	hypara['seed']			= [-1 for _ in range(10)]
	hypara['model']			= ['FCNN','CNN']
	hypara['num_layer']		= [1,2,4,8]
	hypara['num_dim']		= [10,20,40,80]

	if __debug__:
		hypara['script']		= ['experiment/main.py'] 
		hypara['num_data']		= [10]
		hypara['seed']			= [-1 for _ in range(2)]
		hypara['model']			= ['FCNN','CNN']
		hypara['num_layer']		= [1]
		hypara['num_dim']		= [10]
		hypara['num_sample']	= [10]
	
	configs = get_parallel_comb(hypara)
	print(f'trial configs size:{len(configs)}')
	return configs


# ----------------------
import json, os
from collections import namedtuple
def get_resources():
	resource_config = 'experiment/computational_resource_config.json'
	if not os.path.exists(resource_config):
		report('something wrong.')
		report('there is no computationa resource config file.')
		report('please set the file.')
		return
	with open(resource_config,'r') as fp:
		resources = json.load(fp)

	# add a variable for convenience
	resources['num_GPU'] = len(resources['available_GPU_index'])

	# make config immutable
	for key,value in resources.items():
		if isinstance(value, list):
			resources[key] = tuple(value)
	resources = namedtuple('_',resources.keys())(**resources)

	report(f'computational resources')
	report(f'\t{"(key)":<20s}:{"(value)"}')
	for k, v in zip(resources._fields, resources):
		report(f'\t{k:<20s}:{v}')
	print('-'*50)
	return resources

# ------------------------------------------------------
import time
import os
import subprocess
def _pworker(deq,gpu,cpu,_worker_index):
	time.sleep(_worker_index)
	workroom = f'workroom/{_worker_index:02d}/'
	while len(deq)!=0:
		config = deq.popleft()
		
		command = ['nohup','python','-uO', config['script']]
		for key, value in config.items():
			if key=='script':	continue
			if isinstance(value, list):	
				command += [f'--{key}']+values	# equal sign cannot deal with nargs well
			else:
				command += [f'--{key}={value}']
		command += [f'--gpu={gpu}']
		command += [f'--cpu={cpu}']
		command += [f'--workroom={workroom}']

		report(f'remains:{len(deq)} worker-index:{_worker_index} command:{" ".join(command)}')
		os.makedirs(workroom,exist_ok=True)
		with open(f'{workroom}/log.txt','w') as logfile:
			subprocess.call(command, stdout=logfile)

from collections	import deque
from threading		import Thread
def allocate_works(configs, resources):
	deq	 = deque(configs)
	pool = []

	for i in range(resources.num_worker):
		if i<resources.num_GPU:	gpu = resources.available_GPU_index[i]
		else:					gpu = -1
		pool.append(Thread(target=_pworker,args=(deq,gpu,resources.num_CPU_threads,i)))
	for p in pool:
		p.start()
	for p in pool:
		p.join()

# ------------------------------------------------------
import time, os
def main():
	report('start')
	print('-'*50)

	while os.path.exists('workroom'):
		report('waiting...')
		time.sleep(30)

	resources	= get_resources()
	configs		= get_config()
	allocate_works(configs, resources)
	
	if os.path.exists('workroom') and len(os.listdir('workroom'))==0:
		os.rmdir('workroom')

	report('finish')


if __name__ == '__main__':
	main()



# ----------------------
