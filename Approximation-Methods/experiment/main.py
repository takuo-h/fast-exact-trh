#! -*- coding:utf-8 -*-

import torch
torch.backends.cudnn.deterministic	= True
torch.backends.cudnn.benchmark		= False

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import model_zoo as zoo
def get_model(config):
	if config.model=='FCNN':	model = zoo.FCNN(layer=config.num_layer, dim=config.num_dim)
	if config.model=='CNN':		model = zoo.CNN(layer=config.num_layer, dim=config.num_dim)
	if 0<=config.gpu:
		model = model.cuda(config.gpu)
	return model

import torch
from collections import namedtuple
def get_data(config):
	x = torch.rand(config.num_data,1,28,28)
	y = torch.randint(low=0,high=10,size=(config.num_data,))
	if 0<=config.gpu:
		x = x.cuda(config.gpu)
		y = y.cuda(config.gpu)
	return namedtuple('_','x y n')(x=x, y=y,n=len(y))

# -------------------------------------------------------------------
import HessianCalculation.Proposal as proposal
from torch.nn.utils import parameters_to_vector
def calculate_proposal(model, data):
	diagH = proposal.fast_exact(data.x, model)
	diagH = parameters_to_vector(diagH.parameters())
	return diagH

# -------------------------------------------------------------------
import time
import copy
import HessianCalculation.Approximation as approximation
from collections import defaultdict
def evaluate_approximation(origin, data, config):
	report('diag[H]')
	performances = defaultdict(list)


	model = copy.deepcopy(origin)

	time_start	= time.perf_counter()
	diagH = calculate_proposal(model, data)
	time_end		= time.perf_counter()
	performances['gold-time'] = time_end - time_start

	methods = {}
	methods['arXiv'] 	= approximation.tsuzuku_arxiv	#def tsuzuku_arxiv(data, model, num_sample=10, r=1e-5):
	methods['ICML'] 	= approximation.tsuzuku_icml	#def tsuzuku_icml(data, model, num_sample=10, r=1e-5, epsilon=1e-8):

	for name,method in methods.items():
		for r in [1e-6,1e-7,1e-8,1e-9]:
			report(f'start method:{name} r:{r}')
			model = copy.deepcopy(origin)
			for estimated, elapsed_time in method(data, model, num_sample=config.num_sample, r=r):
				diff = (estimated - diagH)
				sgpu = {}
				sgpu['RMS'] 		= diff.pow(2).mean().sqrt().item()
				sgpu['L1-mean']	= diff.abs().mean().item()
				sgpu['max-norm']	= diff.abs().max().item()
				sgpu['time']		= elapsed_time
				performances[name,r].append(sgpu)

	return performances

# ------------------------------------------------------
import argparse
import datetime, time
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--workroom',			default='workroom',	type=str)
	args.add_argument('--archive',								type=str,	default=f"ARCHIVEs/{datetime.datetime.now().strftime('%Y-%m-%d=%H-%M-%S-%f')}")
	args.add_argument('--seed',				default=0,			type=lambda x: int(x) if x.isdecimal() else int(time.time()) )
	args.add_argument('--gpu',				default=-1,			type=lambda x: int(x) if (torch.cuda.is_available() and 0<=int(x)<torch.cuda.device_count()) else -1)
	args.add_argument('--cpu',				default=1,			type=int)
	args.add_argument('--num_data',			default=3,			type=int)	# need to use same data
	# model configuration
	args.add_argument('--model',			default='CNN',		type=str, choices=['CNN','FCNN'])
	args.add_argument('--num_layer',		default=1,			type=int)
	args.add_argument('--num_dim',			default=2,			type=int)
	# approximation configuration
	args.add_argument('--num_sample',		default=10000,		type=int)

	args = vars(args.parse_args())	
	return namedtuple('_',args.keys())(**args)

# -------------------------------------------------------------------	
import time
import random
import numpy
import torch
def set_random_seed(config):
	random.seed(config.seed)
	numpy.random.seed(config.seed)
	torch.manual_seed(config.seed)	
	report(f'set random-seed:{config.seed}')

import torch
def set_device(config):
	report(f'set computational config')
	report(f'\tnumber of CPU thread:{config.cpu}')
	report(f'\tGPU-idex:{config.gpu}')
	# change default GPU just to be sure.
	if config.gpu!=-1:
		torch.cuda.set_device(config.gpu)
	# set CPU-thread setting
	torch.set_num_threads(config.cpu)
	torch.set_num_interop_threads(config.cpu)

# -------------------------------------------------------------------	
import os, json, pickle
def main():
	# setup
	config	= get_config()
	
	set_random_seed(config)
	set_device(config)

	model = get_model(config)
	data  = get_data(config)

	# evaluate
	perfs = evaluate_approximation(model, data, config)
		
	# record results
	os.makedirs(config.workroom, exist_ok=True)	
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/perfs.pkl', 'wb') as fp:
		pickle.dump(perfs, fp)

	os.makedirs(config.archive,	exist_ok=True)
	os.rename(config.workroom,	config.archive)

# -------------------------------------

if __name__ == '__main__':
	main()


# -------------------------------------