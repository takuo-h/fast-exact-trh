#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
from torch.nn.utils import parameters_to_vector
from torch.nn.functional import cross_entropy
def _get_gradient(data, model):
	model.zero_grad()
	loss = cross_entropy(model(data.x),data.y,reduction='sum')
	loss.backward()
	for param in model.parameters():
		param.data = param.grad.data
	grad = parameters_to_vector(model.parameters())
	return grad.detach()

# -------------------------------------------------------------------
import torch
import copy
from torch.nn.utils import parameters_to_vector
from torch.nn.utils import vector_to_parameters
import time
def tsuzuku_arxiv(data, model, num_sample=10, r=1e-5):
	origin = model.parameters()
	origin = parameters_to_vector(origin)
	origin = copy.deepcopy(origin.detach())

	accumulate_diagH = torch.zeros_like(origin)
	for i in range(num_sample):
		time_start	= time.perf_counter()

		epsilon = torch.randn_like(origin)

		plus_param  = origin + r*epsilon
		vector_to_parameters(plus_param, model.parameters())
		plus_grad = _get_gradient(data, model)

		minus_param = origin - r*epsilon
		vector_to_parameters(minus_param, model.parameters())
		minus_grad = _get_gradient(data, model)

		accumulate_diagH += epsilon*(plus_grad - minus_grad)/(2*r)

		time_end		= time.perf_counter()
		elapsed_time	= time_end - time_start

		estimated = accumulate_diagH / (i+1)
		yield estimated, elapsed_time

	vector_to_parameters(origin, model.parameters())

# -------------------------------------------------------------------
import torch
import copy
from torch.nn.utils import parameters_to_vector
from torch.nn.utils import vector_to_parameters
from torch.nn.functional import cross_entropy
import copy
import time
def tsuzuku_icml(data, model, num_sample=10, r=1e-5, epsilon=1e-8):
	origin = model.parameters()
	origin = parameters_to_vector(origin)
	origin = copy.deepcopy(origin.detach())

	accumulate_diagH = torch.zeros_like(origin)
	for i in range(num_sample):

		time_start	= time.perf_counter()

		# random sample s ~ {-1,+1}^n
		s = torch.randint_like(origin,low=0,high=2)*2-1

		plus_param  = origin + r * (  origin.abs()*s + epsilon )
		vector_to_parameters(plus_param, model.parameters())
		plus_grad = _get_gradient(data, model)

		minus_param = origin + r * ( -origin.abs()*s + epsilon )
		vector_to_parameters(minus_param, model.parameters())
		minus_grad = _get_gradient(data, model)

		accumulate_diagH += s * (plus_grad - minus_grad) / (2*r*origin.abs()+epsilon)

		time_end		= time.perf_counter()
		elapsed_time	= time_end - time_start
		
		estimated = accumulate_diagH / (i+1)
		estimated = estimated.clamp(min=0)
		yield estimated, elapsed_time

	vector_to_parameters(origin, model.parameters())




# -------------------------------------------------------------------
