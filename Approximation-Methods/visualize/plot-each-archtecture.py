#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
import math
import numpy as np
import os
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
import matplotlib.lines as mlines

from matplotlib import pyplot as plt
plt.rcParams['text.usetex'] = True

from matplotlib.legend_handler import HandlerBase
class DoubleLineHandler(HandlerBase):
	def create_artists(self, legend, orig_handle, x0, y0, width, height, fontsize, trans):
		line1 = plt.Line2D([x0+width/6,y0+width*7/8], [0.5*height,0.5*height],color=orig_handle[0], lw=6, alpha=0.2)
		line2 = plt.Line2D([x0,y0+width],             [0.5*height,0.5*height],color=orig_handle[0], lw=1, alpha=1.0)
		return [line1, line2]

class SingleLineHandler(HandlerBase):
	def create_artists(self, legend, orig_handle, x0, y0, width, height, fontsize, trans):
		line1 = plt.Line2D([x0,y0+width],             [0.5*height,0.5*height],color=orig_handle[0], lw=1, alpha=1.0)
		return [line1,]

def sumup(buckets, file_name):
	print(f'draw:{file_name}')

	fig = plt.figure(figsize=(8,3))
	plt.subplots_adjust(left=0.1, right=0.97, top=0.87, bottom=0.15, wspace=0.2, hspace=0.1)
				
	for i,n in enumerate([10,100,1000]):
		for j,method in enumerate(['arXiv','ICML']):
			ax = fig.add_subplot(2,3,j*3+i+1)

			prop_time = [t['gold-time'] for t in buckets[n]]
			prop_time = np.array(prop_time)
			px = np.mean(prop_time)

			ax.axvline(x=px,color='black',lw=1.5,linestyle='-', zorder=10)

			time_max, time_min = -math.inf,+math.inf
			for k,r in enumerate([1e-6,1e-7,1e-8,1e-9]):
				xs,ys = [],[]
				for trial in buckets[n]:
					x,y = [0],[]
					for score in trial[method,r]:
						x.append(x[-1] + score['time'])
						y.append(score['RMS'])
					x = x[1:] # or x[:-1]
					xs.append(x)
					ys.append(y)

				xs = np.array(xs)
				ys = np.array(ys)

				time_min = min(time_min, xs.min())
				time_max = max(time_max, xs.max())

				ys = np.log(ys)

				x = np.mean(xs,axis=0)
				y = np.mean(ys,axis=0)
				e = np.std(ys,axis=0)

				ye_min = np.exp(y-e)
				ye_max = np.exp(y+e)
				y = np.exp(y)

				ax.plot(x, y, lw=1.0, color=cm.Set1(k))
				ax.fill_between(x, ye_min, ye_max, color=cm.Set1(k), alpha=0.2, edgecolor='none')

				if k==0: ax.scatter(x[0],y[0],s= 28,marker='D',linewidth=1.7,facecolor='none',edgecolor=cm.Set1(k))
				if k==1: ax.scatter(x[0],y[0],s= 40,marker='o',linewidth=1.7,facecolor='none',edgecolor=cm.Set1(k))
				if k==2: ax.scatter(x[0],y[0],s= 50,marker='s',linewidth=1.3,facecolor='none',edgecolor=cm.Set1(k))
				if k==3: ax.scatter(x[0],y[0],s= 80,marker='x',linewidth=1.2,color=cm.Set1(k))
			ax.set_xscale('log')
			ax.set_yscale('log')
			ax.minorticks_off()

			ax.spines['right'].set_visible(False)
			ax.spines['top'].set_visible(False)
			ax.yaxis.set_ticks_position('left')
			ax.xaxis.set_ticks_position('bottom')

			if n==10:	margin = 0.01
			if n==100:	margin = 0.02
			if n==1000:	margin = 0.03
			ax.text(0.83-margin,0.9, f'{method},n={n}',zorder=20,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,backgroundcolor='white')

			ax.set_xlim(time_min/2, time_max)

			xticks = [1e-2,1e-1,1e-0,1e+1]
			for L in xticks:
				ax.axvline(x=L,color='black',zorder=0,lw=0.5,linestyle='--')
			
			if j==0:
				ax.set_xticks([])
				ax.set_xticklabels([])
			if j==1:
				ax.set_xticks(xticks)
				ax.set_xticklabels(xticks)
				ax.set_xlabel(r'Elapse Time (sec)',fontsize=10)
			if i==0:
				ax.set_ylabel(r'Root Mean Square',fontsize=9)

			if i==0 and j==0:
				colors = [(cm.Set1(v),) for v in [0,1,2,3]]
				labels = [r'r$=10^{-6}$',r'r$=10^{-7}$',r'r$=10^{-8}$',r'r$=10^{-9}$']
				legend1 = ax.legend(colors, labels, handler_map={tuple: DoubleLineHandler()}, ncol=10, loc=(0.0, 1.12), fontsize=9, columnspacing=0.8)

				handles = [mlines.Line2D([0],[0],color='black', label=r'Averaged Time of Proposal (with Exact Estimation)')]
				legend2 = ax.legend(handles=handles, ncol=1, loc=(1.9, 1.12), fontsize=9)

				ax.add_artist(legend1)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
import math
from collections import defaultdict
def main():
	report('start draw')

	archive = '+MIRROR/ARCHIVEs/'

	keywords = ['model','num_layer','num_dim','num_data']
	
	buckets = {}
	for d in os.listdir(archive):
		if '.DS' in d: continue

		with open(f'{archive}/{d}/perfs.pkl','rb') as fp:
			perfs = pickle.load(fp)

		with open(f'{archive}/{d}/config.json','r') as fp:
			config = json.load(fp)
		*keys,n = [config[k] for k in keywords]
		keys = tuple(keys)

		if keys not in buckets: buckets[keys] = defaultdict(list)
		buckets[keys][n].append(perfs)

	for keys in buckets:
		m,L,d = keys
		file_name = f'+VIEW/model={m}/layer={L},dim={d}.png'
		sumup(buckets[keys], file_name)

	report('finish')	
	print('-'*50)


# -------------------------------------------------------------------
if __name__=='__main__':
	main()


