1 What is the Aim of These Codes?
====
1. Provide codes approximating tr[H].
2. Provide experimental results how they work well. 


Especially, we focus on following two approximation methods to calculate tr[H].
1. [Normalized Flat Minima- Exploring Scale-Invariant Definition of Flat Minima for Neural Networks using PAC-Bayesian Analysis](https://arxiv.org/abs/1901.04653)\
	Arxiv, last revised 28 Jan 2019 (this version, v2)
2. [Normalized Flat Minima- Exploring Scale Invariant Definition of Flat Minima for Neural Networks Using PAC-Bayesian Analysis](http://proceedings.mlr.press/v119/tsuzuku20a.html)\
	International Conference on Machine Learning (ICML) 2020


2 Explanation of the Approximation Methods
====

2-1 Arxiv Version
----
[Normalized Flat Minima- Exploring Scale-Invariant Definition of Flat Minima for Neural Networks using PAC-Bayesian Analysis](https://arxiv.org/abs/1901.04653)\
Arxiv, last revised 28 Jan 2019 (this version, v2)

They exploit equation 60 and 61 in page 24 in the paper.

```math
	\mathbb{E}_{\epsilon \sim \mathcal{N}(\mathbf{0},\mathbb{I})} 
		[
			\epsilon \odot \nabla^2 L(\theta) \epsilon
		]
	\approx
	\mathbb{E}_{\epsilon \sim \mathcal{N}(\mathbf{0},\mathbb{I})} 
		\left[
			\epsilon \odot 
			\frac
				{\nabla L(\theta+r\epsilon)-\nabla L(\theta-r\epsilon)}
				{2r}
		\right]
```
where $`\odot`$ is elementwise product operation. \
How to choose the hyper-parameter $`r`$: 
> In our experiments, r was chosen per weight matrix according to their Frobenius norm for better estimation of the Hessian


2-2 ICML Version 
---
[Normalized Flat Minima- Exploring Scale Invariant Definition of Flat Minima for Neural Networks Using PAC-Bayesian Analysis](http://proceedings.mlr.press/v119/tsuzuku20a.html)\
	International Conference on Machine Learning (ICML) 2020

They exploit equation 29,30, and 31 in page 6 in the paper.

```math
	f 
		=
		\mathbb{E}_{\mathbf{s} \in \{-1,+1\}^n} 
		\left[
			\mathbf{s} 
			\odot 
			(
				g(+\mathbf{s}) - g(-\mathbf{s})
			)
			\oslash
			2r 
			(
				\text{abs}(\theta) + \epsilon \mathbb{1}
			)
		\right]
```
where $`\oslash`$ is elementwise divide operation
and 
$`
	g(\mathbf{s}) 
	=
	\nabla L(\theta +  r (\text{abs}(\theta)\odot \mathbf{s} + \epsilon \mathbb{1}))
`$. \
They finally utilize $`\hat{f} = \text{ReLU}(f)`$ as approximation of diagonal Hessian. \
In the paper, it seems there is no description how to choose $`r`$ and $`\epsilon`$.

 

3 Experiments
====

Setups
----
All experiments tried out following settings 10 times.\
Note these settings are completely same in evaluating proposals.

+ Architecture: CNN, FCNN
+ Number of Layers: 1,2,4,8
+ Number of Hidden Dimension: 10, 20, 40, 80
+ Number of Data Size(n): 10, 100, 1000

Visualizing Results 
-----
Examples of exmerilemtal results are plotted as follows:


| Architecture:CNN(layer=1,dim=10) | Architecture:CNN(layer=8,dim=80) |
|:---------:|:---------:|
|<img src="VIEW/model=CNN/layer=1,dim=10.png" width="400" height="180">|<img src="VIEW/model=CNN/layer=8,dim=80.png" width="400" height="180">|

| Architecture:FCNN(layer=1,dim=10) | Architecture:FCNN(layer=8,dim=80) |
|:---------:|:---------:|
|<img src="VIEW/model=FCNN/layer=1,dim=10.png" width="400" height="180">|<img src="VIEW/model=FCNN/layer=8,dim=80.png" width="400" height="180">|


+ the x-axis denotes elapse time 
+ the y-axis denotes root mean squreare error 
	between 
		$`\text{diag}[\bold{H}]_{\text{app}}`$
		and 
		$`\text{diag}[\bold{H}]_{\text{prop}}`$,\
	i.e., 
	$`\frac{1}{\sqrt{d}} \| \text{diag}[\bold{H}]_{\text{app}} - \text{diag}[\bold{H}]_{\text{prop}}\|_2 `$ for $`\theta \in \mathbb{R}^d`$.
+ Each line shows averaged over 10 time trials and colored area does its' standard deviation.
+ Vertial black line indicates proposal time
+ There are other plots with different settings [in directory](https://gitlab.com/takuo-h/fast-exact-trh/-/tree/main/Approximation-Methods/VIEW). See them, if you want to know more.


Concolusion
-----
Obviously, previous methods have limited ability to approximate $`\text{diag}[\bold{H}]`$.\
In addition, the approximation methods have hyper-parameters which costs additionally..

