***30 Seconds Overview***
====
This repository introduces a script
	which calculates $`\text{tr}[\bold{H}]`$ and $`\text{diag}[\bold{H}]`$
	**exactly and significantly faster**.

More specifically, there are three contents.
1. [Experiments: How Exact and Fast](https://gitlab.com/takuo-h/fast-exact-trh#1-experiments-how-exact-and-fast)
2. [Explanation: Why the Script Works](https://gitlab.com/takuo-h/fast-exact-trh#2-explanation-why-proposal-works)
3. [Approximation Approach: How about Previous Methods](https://gitlab.com/takuo-h/fast-exact-trh/-/tree/main/Approximation-Methods)


Notes
----
+ This script heavily depends on an other script which calculates <ins>example-wise gradient super efficiently</ins>.\
	If you're interested in it, [see my other repository](https://gitlab.com/takuo-h/examplewise-gradients).
+ These codes are used in [Minimum sharpness: Scale-invariant parameter-robustness of neural networks](https://arxiv.org/abs/2106.12612) (ICML 2021 workshop)


---
1 **Experiments: How Exact and Fast**
====
How to run: 
```bash
nohup bash run.sh > log.txt &
```
or run experiments and visualization individually as
```bash
python -uO experiment/run.py # to obtain numerical results
python -uO　visualize/run.py  # to generate images using the results
```
Computational resources information is provided in [this section](https://gitlab.com/takuo-h/fast-exact-trh#computational-resources).\
If you omit -O option, it runs with debugging hyper-parameters.


(under experiments. it will be finished in 2021/07/20.)

1.1 **How Exact Does Our Proposal Calculate $`\text{tr}[\bold{H}]`$**
---

Plot two estimations on two-dimensional panels.\
Here, a dot $`(x,y)`$ denotes $`(\text{tr}[\bold{H}]_{\text{prop}},\text{tr}[\bold{H}]_{\text{naive}})`$, where $`\text{tr}[\bold{H}]_{\text{prop}}`$ obtained from proposed calculation and so on.\
If a dot is on the diagonal line $`x=y`$, it indicates two estimations are equal, and thus my scripts correctly calculate $`\text{tr}[\bold{H}]`$.\
In any case, all dots are on the diagonal line.

| Total-Result (with-GPU) | Total-Result (with-CPU) |
|:---------:|:---------:|
|<img src="results/with-GPU/VIEW/how-exact-proposal.png" width="150" height="150">|<img src="results/with-GPU/VIEW/how-exact-proposal.png" width="150" height="150">|



1.2 **How Our Proposal Accelerates Calculating $`\text{tr}[\bold{H}]`$**
---

Plot how much time do naive and proposed methods take.\
Note that upper panels (naive methods) have log-scale, while lower ones (proposal) has linear-scale.\
Obviously, my script significantly accelerates the calculation.

| Details (CNN w GPU) | Details (FCNN w GPU) |
|:---------:|:---------:|
|<img src="results/with-GPU/VIEW/how-accelerate-calculation-with-CNN.png" width="300" height="150">|<img src="results/with-GPU/VIEW/how-accelerate-calculation-with-FCNN.png" width="300" height="150"> |


| Details (CNN w CPU) | Details (FCNN w CPU) |
|:---------:|:---------:|
|<img src="results/with-CPU/VIEW/how-accelerate-calculation-with-CNN.png" width="300" height="150">|<img src="results/with-CPU/VIEW/how-accelerate-calculation-with-FCNN.png" width="300" height="150"> |


2 **Explanation: Why Proposal Works**
====

2.1 **Notations**
---

Firstly, we introduce standard notations including dataset, loss function and it's Hessian.

+ Dataset: $`\mathcal{D}=\{(\mathbf{x}_i,y_i)\}_{i=1}^n`$.
+ Loss function over the dataset: $`L=\sum_{i} \ell(\mathbf{x}_i,y_i;\theta)`$. \
	In many cases, we omit $`\theta`$ (model parameters) for simplicity.
+ Hessian of the loss: $`\bold{H}=\frac{\partial^2 }{\partial \theta^2} L = \Delta L`$.\
	For simplicity, we use laplace operator symbol, $`\Delta=\frac{\partial^2 }{\partial \theta^2}`$.


In this repository, we focus on neural networks whose activation functions are only s non-negative homogeneous (NNH) function.\
Here, NNH function $`\sigma`$ satisfies a condition: $`\sigma(\alpha \mathbf{x})=\alpha \sigma(\mathbf{x})`$ for any input $`\mathbf{x}$$ and scale $$\alpha \in \mathbb{R}_{+}`$.\
ReLU and Maxout are important instantiation of NNH function.\
Because many architectures employ them, this assumption would be widely acceptable in general.


2.2 **Decomposition Formulation**
---
My script relies on following decompositon for NNs with NNH activations.

+ $` \text{tr}[\bold{H}] 
		= 
		\displaystyle{\sum_{(\mathbf{x},y)}^{\mathcal{D}}}
		\text{score}(\mathbf{x})
`$,
$`\qquad \text{score}(\mathbf{x}) = 
		\sum_{k}
		p_k \|\frac{\partial}{ \partial \theta} o_k \|_2^2 
		- \| \frac{\partial}{\partial \theta} \ln Z \|_2^2
`$\
where $`o_k`$ is logit for $`k`$-th class,
	$`Z=\sum_k o_k`$,
	and 
	$`p_k`$ is softmax of $`o_k`$.


This decomposition has two benefits:
1. work within standard back-propagation framework (including mini-batch).
2. require only $`K+1`$ times back-propagations ($`K`$ is number of classes).

These benefits enable us to calculate $`\text{tr}[\bold{H}]`$ exactly and significantly faster, as shown in [Experiments: How Exact and Fast](https://gitlab.com/takuo-h/fast-exact-trh#1-experiments-how-exact-and-fast)\
Note that this decomposition need samplewise gradient calculation. If you want to know it, [see other my repository](https://gitlab.com/takuo-h/examplewise-gradients)\
Following sections introduce three steps to explain why the decomposition works.


**1st Step: From Dataset to ExampleWise**
----
Based on following two equations, we decompose Hessian over datasets to examplewise ones.\
Here, we define samplewise Hessian, $`\bold{H}_i = \Delta \ell(\mathbf{x}_i,y_i)`$ for convenience. 

1. $` \bold{H}	= \Delta L
				= \Delta \sum_{i} \ell(\mathbf{x}_i,y_i)
				= \sum_{i} \Delta \ell(\mathbf{x}_i,y_i)
				= \sum_{i} \bold{H}_i
	`$\
	$`\therefore \Delta (f_1+f_2) = \Delta f_1 + \Delta f_2`$
2. $`\text{tr}[\bold{H}] 
	= \text{tr}[ \sum_{i} \bold{H}_i] 
	= \sum_{i} \text{tr}[ \bold{H}_i] 
	`$\
	$`\therefore \text{tr}[\bold{A}+\bold{B}]=\text{tr}[\bold{A}]+\text{tr}[\bold{B}]`$


This decomposition implies a first important observation: 
	***To calculate $`\text{tr}[\bold{H}]`$, just sum up $`\text{tr}[\bold{H}_i]`$.***\
Hereafter, if there is no ambiguity, we omit $`i`$ to refer examplewise Hessian $`\bold{H}_i`$ for simplicity.

**2nd Step: From Full-Layers to LayerWise**
---
Because trace operation sums up only diagonal elements, \
we have $`\text{tr}[\bold{H}] = \sum_{l} \text{tr}[\bold{H}_l]`$
	where $`\bold{H}_l`$ is diagonal block of Hessian matrix $`\bold{H}`$ for $`l`$-th layer.

The layerwise Hessian can be written in recurvesive form:
+ $`\bold{H}_l = \bold{P}_l \otimes (\mathbf{x}_l \mathbf{x}_l^{\top}) `$\
	where $`\mathbf{x}_l`$ is an input vector for $`l`$-th layer
	and $`\otimes`$ is Kronecker product.
+ $`\bold{P}_l = 
		\text{diag}(\mathbf{x}_l') 
			\bold{W}_{l+1} 
				\bold{P}_{l+1} 
			\bold{W}_{l+1}^{\top} 
		\text{diag}(\mathbf{x}_l') +
		\text{diag}(
				\mathbf{x}_l'' \odot \frac{\partial \ell}{\partial \mathbf{x}_l} 
			)
	`$\
	where $`\text{diag}`$ is diagonal embedding operation
	and
	$`\mathbf{x}_l'`$ is elementwise deviation.
+ $`\bold{P}_L = \text{diag}[\mathbf{p}]- \mathbf{p}\mathbf{p}^{\top}`$\
	where $`\mathbf{p}`$ is a vector containing $`p_k`$.

If you want to know this recurvesive form in details, see previous work [^1] [^2].



**3rd Step: From Hessian to Gradient**
---
This section introduces following decompositon for NNs with NNH activation function:
+ $`\text{tr}[\bold{H}_l] = 
	\sum_k p_k \| \frac{\partial}{\partial \bold{W}_l} o_k \|_F - \| \frac{\partial }{\partial \bold{W}_l} \ln Z \|_F`$


The key is NNH activation function which simplifies the recurvesive form in [2nd step](https://gitlab.com/takuo-h/fast-exact-trh#2-2-2-2nd-step-from-full-layered-to-layerwise) and reduces required information from second to first order deviation. 
That is, it leads a relation
	$`\mathbf{x}_l''=0`$ 
	and then yield
	$` \bold{H}_l = 
	(\bold{J}_l \bold{W}_L \bold{P}_L \bold{W}_L^{\top} \bold{J}_l^{\top})
	\otimes
	(\mathbf{x}_l \mathbf{x}_l^{\top})
	`$
	where $`\bold{J}_l`$ is some Jacobian matrix.
We transform this formula as follows:


1. $`\text{tr}[\bold{H}_l] 
	= \text{tr}[
		(\bold{J}_l \bold{W}_L \bold{P}_L \bold{W}_L^{\top} \bold{J}_l^{\top})
		\otimes
		(\mathbf{x}_l \mathbf{x}_l^{\top})
		]
	=  \text{tr}[\bold{J}_l \bold{W}_L \bold{P}_L \bold{W}_L^{\top} \bold{J}_l^{\top}] 
		\|\mathbf{x}_l\|_2^2
	`$\
	Here, this equation exploits properties of $`\text{tr}[\cdot]`$,\
	i.e., 
		$`\text{tr}[\bold{A}\otimes\bold{B} ] = \text{tr}[\bold{A}]\text{tr}[\bold{B}]`$ 
		and 
		$`\text{tr}[\bold{A}\bold{B}] = \text{tr}[\bold{B}\bold{A}]`$.

2. $` \text{tr}[\bold{J}_l \bold{W}_L \bold{P}_L \bold{W}_L^{\top} \bold{J}_l^{\top}] 
	= \text{tr}[
			\bold{J}_l \bold{W}_L 
			(\text{diag}[\mathbf{p}] - \mathbf{p}\mathbf{p}^{\top})
			\bold{W}_L^{\top} \bold{J}_l^{\top}]
	`$\
	$` = \text{tr}[ 
			\bold{J}_l \bold{W}_L 
			\text{diag}[\mathbf{p}] 
			\bold{W}_L^{\top} \bold{J}_l^{\top}]
		- \| \bold{J}_l \bold{W}_L  \mathbf{p} \|_2^2
	`$\
	$` = \sum_{k} \mathbf{p}_k \|\bold{J}_l \mathbf{w}_k \|_2 ^2
		- \| \bold{J}_l \bold{W}_L \mathbf{p} \|_2^2
	`$\
	where $`\mathbf{w}_k`$ is a vector in $`\bold{W}_L`$ for predicting $k$-th class.
3. $` \text{tr}[\bold{H}_l] = 
		\left(
			\sum_{k} \mathbf{p}_k \|\bold{J}_l \mathbf{w}_k \|_2 ^2
			- \| \bold{J}_l \bold{W}_L \mathbf{p} \|_2^2		
		\right)
		\|\mathbf{x}_l\|_2^2
	`$\
	$` = 	\sum_{k} \mathbf{p}_k \| (\bold{J}_l \mathbf{w}_k) \mathbf{x}_l^{\top} \|_F ^2
			- \| (\bold{J}_l \bold{W}_L \mathbf{p} ) \mathbf{x}_l^{\top} \|_F^2		
	`$\
	$` = 	\sum_{k} \mathbf{p}_k \| \frac{\partial }{\partial \bold{W}_l } o_k  \|_F^2
			- \| \frac{\partial }{\partial \bold{W}_l } \ln Z \|_F^2
	`$\
	Here, we exploit two relations,
		$` \frac{\partial}{\partial \bold{W}_l} o_k =
		(\bold{J}_l \mathbf{w}_k) \mathbf{x}_l^{\top}
		`$
		and 
		$` \frac{\partial}{\partial \bold{W}_l } \ln Z =
		(\bold{J}_l \bold{W}_L \mathbf{p}) \mathbf{x}_l^{\top}
		`$.

2.3 **Summing Up All Steps**
----
Pluging two equations in 2nd and 3rd steps, we have
+ $`\text{score}(\mathbf{x}) = \text{tr}[\bold{H}] = \sum_{l} \text{tr}[\bold{H}_l] = \sum_{l} \sum_{k} \mathbf{p}_k \| \frac{\partial }{\partial \bold{W}_l } o_k  \|_2 ^2 - \| \frac{\partial }{\partial \bold{W}_l } \ln Z \|_2^2`$\
	$`= \sum_{k} \mathbf{p}_k \| \frac{\partial }{\partial \theta } o_k  \|_2 ^2 - \| \frac{\partial }{\partial \theta } \ln Z \|_2^2 `$




3 **Appendix**
====

Approximation Approach: How about Previous Methods
---

[see this directory](https://gitlab.com/takuo-h/fast-exact-trh/-/tree/main/Approximation-Methods)

Computational Resources (GPU)
---
The experiments used following computational resorces
+ Server: Ubuntu Linux server
+ CPU: Xeon Gold 5222 CPUs
+ GPU: Tesla V100 SXM2 32GB GPU
+ Nvidia driver version: 440.64.00 
+ CUDA version: 481 10.2
+ python version: 3.9.1
+ PyTorch version: 1.9.0

Computational Resources (CPU)
---
The experiments used following computational resorces
+ Macbook

References
---
[^1]: [Higher-order Reverse Automatic Differentiation with emphasis on the third-order](https://arxiv.org/abs/1309.5479)

[^2]: [Practical Gauss-Newton Optimisation for Deep Learning](http://proceedings.mlr.press/v70/botev17a.html).
