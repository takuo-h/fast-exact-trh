#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import copy
import torch
import exgrads as ExGrads
def fast_exact(inputs, model, batch_size=2**10):
	model.eval()

	# for all
	diagH = copy.deepcopy(model)
	for param in diagH.parameters():
		param.data.zero_()

	ExGrads.register(model)
	
	n = len(inputs)
	for minibatch in torch.split(inputs, batch_size):
		output		= model(minibatch)						# := (b,K)

		# for p*p^T part
		Z		= torch.logsumexp(output, dim=1)			# -> (b)
		loss 	= Z.sum()									# -> (1)
		
		loss.backward(retain_graph=True)
		model.zero_grad()
		for param, diag in zip(model.parameters(), diagH.parameters()):
			grad1 = param.grad1**2 							# := (b,*)
			grad1 = torch.sum(grad1,dim=0) 					# -> (*)
			diag.data -= grad1.detach()

		# for diag(p) part
		prob	= torch.softmax(output,dim=1)				# -> (b,K)
		output	= output.sum(dim=0)							# -> (K)
		for k,output_k in enumerate(output):
			output_k.backward(retain_graph=True)
			model.zero_grad()
			for param, diag in zip(model.parameters(), diagH.parameters()):
				grad1 = param.grad1**2						# := (b,*)
				grad1 = grad1.flatten(1,-1)					# -> (b,prod(*))
				grad1 = prob[:,k].flatten() @ grad1 		# -> (prod(*))
				grad1 = grad1.reshape(param.data.shape)		# -> (*)
				diag.data += grad1.detach()
			
	ExGrads.deregister(model)
	return diagH
