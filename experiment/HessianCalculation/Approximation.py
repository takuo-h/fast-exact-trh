#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
from torch.nn.utils import parameters_to_vector
from torch.nn.functional import cross_entropy
def _get_gradient(data, model):
	model.zero_grad()
	loss = cross_entropy(model(data.x),data.y,reduction='sum')
	loss.backward()
	for param in model.parameters():
		param.data = param.grad.data
	grad = parameters_to_vector(model.parameters())
	return grad.detach()

# -------------------------------------------------------------------
import torch
import copy
from torch.nn.utils import parameters_to_vector
from torch.nn.utils import vector_to_parameters
def tsuzuku_arxiv(data, model, num_sample=10, r=1e-5):
	origin = model.parameters()
	origin = parameters_to_vector(origin)
	origin = copy.deepcopy(origin.detach())

	accumulate_diagH = torch.zeros_like(origin)
	for i in range(num_sample):
		epsilon = torch.randn_like(origin)

		plus_param  = origin + r*epsilon
		vector_to_parameters(plus_param, model.parameters())
		plus_grad = _get_gradient(data, model)

		minus_param = origin - r*epsilon
		vector_to_parameters(minus_param, model.parameters())
		minus_grad = _get_gradient(data, model)

		accumulate_diagH += epsilon*(plus_grad - minus_grad)/(2*r)

	return accumulate_diagH/num_sample

# -------------------------------------------------------------------
import torch
import copy
from torch.nn.utils import parameters_to_vector
from torch.nn.utils import vector_to_parameters
def tsuzuku_icml(data, model, num_sample=10, r=1e-5, epsilon=1e-10):
	origin = model.parameters()
	origin = parameters_to_vector(origin)
	origin = copy.deepcopy(origin.detach())

	accumulate_diagH = torch.zeros_like(origin)
	for i in range(num_sample):
		s = torch.randint_like(origin,low=0,high=2)*2-1

		plus_param  = origin + r * ( origin.abs()*   s + epsilon )
		vector_to_parameters(plus_param, model.parameters())
		plus_grad = _get_gradient(data, model)
		
		minus_param = origin + r * ( origin.abs()*(-s) + epsilon )
		vector_to_parameters(minus_param, model.parameters())
		minus_grad = _get_gradient(data, model)

		accumulate_diagH += s * (plus_grad - minus_grad) / (2*r*origin.abs()+epsilon)
	accumulate_diagH = accumulate_diagH.clamp(min=0)
	return accumulate_diagH/num_sample

