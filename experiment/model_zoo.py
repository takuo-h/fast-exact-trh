#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import torch
class CNN(torch.nn.Module):
	def __init__(self, dim, layer):
		super(CNN, self).__init__()
		self.layerI	= torch.nn.Conv2d(1,  dim, 5, stride=1, padding=0)
		self.layers = torch.nn.ModuleList([torch.nn.Conv2d(dim,dim,3,stride=1,padding=1) for _ in range(layer)])
		self.layerO	= torch.nn.Linear( 6*6*dim, 10)

	def forward(self, x):								# := (b, 1,28,28)
		x = torch.relu(self.layerI(x))					# -> (b,dim,24,24)
		x = torch.nn.functional.max_pool2d(x, 2, 2)		# -> (b,dim,12,12)
		for L in self.layers:
			x = torch.relu(L(x))						# -> (b,dim,12,12)
		x = torch.nn.functional.max_pool2d(x, 2, 2)		# -> (b,dim, 6, 6)
		x = x.flatten(1,-1)								# -> (b,dim*6*6)
		return self.layerO(x)

# -------------------------------------------------------------------
import torch
class FCNN(torch.nn.Module):
	def __init__(self, dim, layer):
		super(FCNN, self).__init__()
		self.layerI	= torch.nn.Linear(28*28, dim)
		self.layers = torch.nn.ModuleList([torch.nn.Linear(dim,dim) for _ in range(layer)])
		self.layerO	= torch.nn.Linear(  dim, 10)

	def forward(self, x):					# := (b, 1,28,28)	# (batch, channel, width, height)
		x = x.flatten(1,-1)					# -> (b, 1*28*28)
		x = torch.relu(self.layerI(x))		# -> (b, dim)
		for L in self.layers:				
			x = torch.relu(L(x))			# -> (b,dim)
		return self.layerO(x)				# -> (b,10)

# -------------------------------------------------------------------
