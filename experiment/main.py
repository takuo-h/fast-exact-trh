#! -*- coding:utf-8 -*-

import torch
torch.backends.cudnn.deterministic	= True
torch.backends.cudnn.benchmark		= False

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import model_zoo as zoo
def get_model(config):
	if config.model=='FCNN':	model = zoo.FCNN(layer=config.num_layer, dim=config.num_dim)
	if config.model=='CNN':		model = zoo.CNN (layer=config.num_layer, dim=config.num_dim)
	if 0<=config.gpu:
		model = model.cuda(config.gpu)
	return model

import torch
from collections import namedtuple
def get_data(config):
	x = torch.rand(config.num_data,1,28,28)
	y = torch.randint(low=0,high=10,size=(config.num_data,))
	if 0<=config.gpu:
		x = x.cuda(config.gpu)
		y = y.cuda(config.gpu)
	return namedtuple('_','x y n')(x=x, y=y,n=len(y))

# -------------------------------------------------------------------
import HessianCalculation.Naive    as naive
from torch.nn.functional import cross_entropy
def calculate_naive(model,data):
	loss  = cross_entropy(model(data.x), data.y, reduction='sum')
	diagH = naive.diagonal(loss, model)
	return diagH

import HessianCalculation.Proposal as proposal
from torch.nn.utils import parameters_to_vector
def calculate_proposal(model, data):
	diagH = proposal.fast_exact(data.x, model)
	diagH = parameters_to_vector(diagH.parameters())
	return diagH

import HessianCalculation.Approximation as approximation
def calculate_tsuzuku_arXiv(model, data):
	return approximation.tsuzuku_arxiv(data, model)

import HessianCalculation.Approximation as approximation
def calculate_tsuzuku_ICML(model, data):
	return approximation.tsuzuku_icml(data, model)


# -------------------------------------------------------------------
import time
import copy
def evaluate_diag(origin, data):
	report('diag[H]')

	methods = {}
	methods['naive'] 		= calculate_naive
	methods['proposal'] 	= calculate_proposal
#	methods['arXiv'] 		= calculate_tsuzuku_arXiv
#	methods['ICML'] 		= calculate_tsuzuku_ICML

	elapsed_times	= {}
	estimate_diagH	= {}
	for name,method in methods.items():
		model 		= copy.deepcopy(origin)
		time_start	= time.perf_counter()
		diagH		= method(model, data)
		time_end	= time.perf_counter()

		elapsed_times[name]  = time_end - time_start
		estimate_diagH[name] = diagH.detach()

	trH = {}
	for name in estimate_diagH:
		trH[name] = estimate_diagH[name].sum().item()

	errors = {}
	for name in methods:
		if name=='naive': continue
		diff = estimate_diagH['naive'] - estimate_diagH[name]
		errors[f'{name}-RMS'] 		= diff.pow(2).mean().sqrt().item()
		errors[f'{name}-L1-mean']	= diff.abs().mean().item()
		errors[f'{name}-max-norm']	= diff.abs().max().item()

	return elapsed_times, trH, errors

# ------------------------------------------------------
import argparse
import datetime, time
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--workroom',			default='workroom',	type=str)
	args.add_argument('--seed',				default=0,			type=lambda x: int(x) if x.isdecimal() else int(time.time()) )
	args.add_argument('--gpu',				default=-1,			type=lambda x: int(x) if (torch.cuda.is_available() and 0<=int(x)<torch.cuda.device_count()) else -1)
	args.add_argument('--cpu',				default=1,			type=int)
	args.add_argument('--num_data',			default=10,			type=int)	# need to use same data
	# model configuration
	args.add_argument('--model',			default='CNN',		type=str, choices=['CNN','FCNN'])
	args.add_argument('--num_layer',		default=3,			type=int)
	args.add_argument('--num_dim',			default=10,			type=int)

	args = vars(args.parse_args())	
	return namedtuple('_',args.keys())(**args)

# -------------------------------------------------------------------	
import time
import random
import numpy
import torch
def set_random_seed(config):
	random.seed(config.seed)
	numpy.random.seed(config.seed)
	torch.manual_seed(config.seed)	
	report(f'set random-seed:{config.seed}')

import torch
def set_device(config):
	report(f'set computational config')
	report(f'\tnumber of CPU thread:{config.cpu}')
	report(f'\tGPU-idex:{config.gpu}')
	# change default GPU just to be sure.
	if config.gpu!=-1:
		torch.cuda.set_device(config.gpu)
	# set CPU-thread setting
	torch.set_num_threads(config.cpu)
	torch.set_num_interop_threads(config.cpu)

# -------------------------------------------------------------------	
import os, json
def main():
	# setup
	config	= get_config()

	set_random_seed(config)
	set_device(config)

	model = get_model(config)
	data  = get_data(config)

	# evaluate
	elapsed_times, trH, errors = evaluate_diag(model, data)

	# report results
	for name in elapsed_times:
		message  = []
		message.append(name)
		message.append(f'time:{elapsed_times[name]: 9.4f} sec')
		message.append(f'tr[H]:{trH[name]: 9.4f}')
		report('\t'.join(message))
	
	# record results
	os.makedirs(config.workroom, exist_ok=True)	
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/elapsed-times.json', 'w') as fp:
		json.dump(elapsed_times, fp)
	with open(f'{config.workroom}/trH.json', 'w') as fp:
		json.dump(trH, fp)
	with open(f'{config.workroom}/errors.json', 'w') as fp:
		json.dump(errors, fp)

# -------------------------------------

if __name__ == '__main__':
	main()


# -------------------------------------