 #!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
from os import path
import query_queue_and_parallel as qqp
def main():
	query = qqp.Query()

	query['scripts']		= [path.join(path.dirname(__file__),'main.py')]
	query['model']			= ['FCNN','CNN']

	# CPU version
	query['num_data']		= [2,4,8]
	query['seed']			= [-1 for _ in range(3)]
#	query['num_layer']		= [1,2,4,8]
#	query['num_dim']		= [1,2,4,8]
	query['num_layer']		= [1,2,4]
	query['num_dim']		= [1,2,4]
	
	# GPU version
#	query['num_data']		= [10,100,1000]
#	query['seed']			= [-1 for _ in range(10)]
#	query['num_layer']		= [1,2,4,8]
#	query['num_dim']		= [10,20,40,80]
	
	query.ready()
	query.run(GPU_index=range(8))	

if __name__ == '__main__':
	main()



# ----------------------
